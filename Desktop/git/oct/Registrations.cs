﻿using System;
using System.Collections.Generic;
namespace oct
{
    public class Registrations
    {
        //consists of the following classes
        // student
        // housing
        // budget

        public Student student;
        public Housing housing;
        public Budget budget;

        public Registrations(Student s, Housing h, Budget b)
        {
            student = s;
            housing = h;
            budget = b;
        }

        //printing a summary
        public string PrintSummary()
        {
            string summary = "Please keep this information for your records:<br>";
            summary += "Name: " + student.StudentFirstName + " " + student.StudentLastName + "<br/>";
            summary += "Email: " + student.StudentEmail + "<br/>";
            summary += "Phone Number: " + student.StudentPhoneNumber + "<br/>";
            summary += String.Join(" | ", housing.transportation.ToArray()) + "<br>";
            summary += "You live at: " + housing.StreetNum + " " + housing.Street + " " + housing.PostalCode + "." + "<br/>";
            summary += "You have " + housing.Roommates + " roommate(s)." + "<br/>";
            summary += "Your Monthly total is: " + CalculateBudget().ToString() + "$" + "<br/>";
            summary += "Breakdown: " + "<br/>";
            summary += "On food: " + budget.FoodCost + "$" + "<br/>";
            summary += "On transportation: " + budget.TransportationCost + "$" + "<br/>";
            summary += "On entertainment: " + budget.EntertainmentCost + "$" + "<br/>";
            summary += "On boarding: " + budget.BoardingCost + "$" + "<br/>";
            summary += "On misc expenses: " + budget.MiscCost + "$" + "<br/>";
            return summary;
        }


        //calculate total budget
        public double CalculateBudget()
        {
            double total = (budget.FoodCost + budget.MiscCost + budget.BoardingCost + budget.EntertainmentCost + budget.TransportationCost);

            return total;
        }

        //if student has a budget over 2500 they qualify for grant
        public bool CalculateGrant()
        {
            double total = (budget.FoodCost + budget.MiscCost + budget.BoardingCost + budget.EntertainmentCost + budget.TransportationCost);

            return total > 2500 ? true : false;
        }
    }
}
