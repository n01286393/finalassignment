﻿using System;
using System.Collections.Generic;
namespace oct
{
    public class Student
    {
        private string studentFirstName;
        private string studentLastName;
        private string studentPhoneNumber;
        private string studentNumber;
        private string studentEmail;

        public Student()
        {

        }

        public string StudentFirstName
        {
            get { return studentFirstName; }
            set { studentFirstName = value; }
        }
        public string StudentLastName
        {
            get { return studentLastName; }
            set { studentLastName = value; }
        }
        public string StudentPhoneNumber
        {
            get { return studentPhoneNumber; }
            set { studentPhoneNumber = value; }
        }
        public string StudentNumber
        {
            get { return studentNumber; }
            set { studentNumber = value; }
        }
        public string StudentEmail
        {
            get { return studentEmail; }
            set { studentEmail = value; }
        }

    }
}
