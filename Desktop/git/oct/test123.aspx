﻿<%@ Page Language="C#" Inherits="oct.test123" %>
<!DOCTYPE html>
<html>
<head runat="server">
	<title>Humber Grant Registration</title>
    <link href="pagestyling.css" rel="stylesheet">
</head>
<body>
    <form id="registration" runat="server">
        <h1>MyHumber Grant</h1>
            <p id="info" runat="server"></p>
            <fieldset>
                <legend>General Personal Information</legend>
                
                <h2>Help us get to know you!</h2>
                
                <label>First Name</label>
                <asp:TextBox ID="studentFirstName" runat="server"></asp:TextBox><br>
                <asp:RequiredFieldValidator ID="studentFirstNameValidator" runat="server" ControlToValidate="studentFirstName"   
ErrorMessage="Please enter your first name" ForeColor="Red"/><br>
                
                <Label>Last Name:</Label>
                <asp:TextBox id="studentLastName" runat="server"></asp:TextBox><br>
                <asp:RequiredFieldValidator ID="studentLastNameValidator" runat="server" ControlToValidate="studentLastName" ErrorMessage="Please enter your last name" ForeColor="Red"/><br>
                
                <asp:Label runat="server" Text="Phone Number"></asp:Label>
                <asp:TextBox id="studentPhoneNumber" runat="server" placeholder="e.g.(647)-999-9999"/><br>
                <asp:RequiredFieldValidator ID="studentPhoneNumberValidator" runat="server" ControlToValidate="studentPhoneNumber"   
ErrorMessage="Please enter your phone number" type="integer" ForeColor="Red"/>  
                
                <br>
                
                <asp:Label runat="server" Text="Student Number"></asp:Label>
                <asp:TextBox id="studentNumber" runat="server" placeholder="e.g.N12345678"></asp:TextBox><br>
                <asp:RequiredFieldValidator ID="studentNumberValidator" runat="server" ControlToValidate="studentNumber"   
ErrorMessage="Please enter your student number" ForeColor="Red"/><br>
                
                <asp:Label runat="server" Text="Email"></asp:Label>
                <asp:TextBox id="studentEmail" runat="server"></asp:TextBox><br>
                <asp:RequiredFieldValidator ID="studentEmailValidator" runat="server" ControlToValidate="studentEmail"   
ErrorMessage="Please enter your preferred email number" type="integer" ForeColor="Red"/><br>
                <asp:RegularExpressionValidator ID="emailRegularExpressionValidator" runat="server" ControlToValidate="studentEmail"   
ErrorMessage="Sorry, that is not a valid email" ForeColor="Red"ValidationExpression="\w+([-+.']\w+)*@\w+([-.]\w+)*\.\w+([-.]\w+)*"/>
                
                <br>
                
                <asp:Label runat="server" Text="Please Retype Email:"></asp:Label>
                <asp:TextBox id="studentEmailRetype" runat="server"></asp:TextBox><br>
                <asp:CompareValidator ID="emailCompareValidator" runat="server" ControlToCompare="studentEmail"   
ControlToValidate="studentEmailRetype" Display="Dynamic" ErrorMessage="Please center the correct email" ForeColor="Red"   
Operator="Equal" Type="String"/>
                
                <br>
            </fieldset>
            
            <fieldset>
                <legend>Housing Information</legend>
                <h2>Please tell us about your living arrangements:</h2>
                
                <Label>Street Number</Label>
                <asp:TextBox id="streetNum" runat="server"></asp:TextBox><br>
                <asp:RequiredFieldValidator ID="streetNumValidator" runat="server" ControlToValidate="streetNum"   
ErrorMessage="Please enter your home number" type="integer" ForeColor="Red"/>  
                
                <br>
                
                <Label>Street</Label>
                <asp:TextBox id="street" runat="server"></asp:TextBox><br>
                <asp:RequiredFieldValidator ID="streetValidator" runat="server" ControlToValidate="street"   
ErrorMessage="Please enter your street name" ForeColor="Red"/>  
                
                <br>
                
                <Label>Postal Code</Label>
                <asp:TextBox id="postalCode" runat="server"></asp:TextBox><br>
                <asp:RequiredFieldValidator ID="postalCodeValidator" runat="server" ControlToValidate="postalCode"   
ErrorMessage="Please enter your postal code" ForeColor="Red"/><br>
                <asp:RegularExpressionValidator ID="postalCodeRegularExpressionValidator" runat="server" ControlToValidate="postalCode"   
ErrorMessage="Sorry, that is not a valid postal code" ForeColor="Red" ValidationExpression="[ABCEGHJKLMNPRSTVXY][0-9][ABCEGHJKLMNPRSTVWXYZ] ?[0-9][ABCEGHJKLMNPRSTVWXYZ][0-9]"/>
              
                <br>
                 <Label>Method(s) of transportation that you use to travel to Humber:</Label><br>
                <div id="transpo_container" runat="server">
                    <asp:CheckBox id="transportation1" runat="server" Text="Subway"></asp:CheckBox>
                    <asp:CheckBox id="transportation2" runat="server" Text="Bus"></asp:CheckBox>
                    <asp:CheckBox id="transportation3" runat="server" Text="Bike"></asp:CheckBox>
                    <asp:CheckBox id="transportation4" runat="server" Text="Walk"></asp:CheckBox>
                    <asp:CheckBox id="transportation5" runat="server" Text="Car"></asp:CheckBox>
                    <asp:CheckBox id="transportation6" runat="server" Text="Train"></asp:CheckBox>
                </div>  
                <br>
                <asp:Label runat="server" Text="How many roommates do you have?"></asp:Label><br>
                <asp:DropDownList id="roommateList" runat="server">
                    <asp:ListItem Value="0">0</asp:ListItem>
                    <asp:ListItem Value="1">1</asp:ListItem>
                    <asp:ListItem Value="2">2</asp:ListItem>
                    <asp:ListItem Value="3">3</asp:ListItem>
                    <asp:ListItem Value="4">4</asp:ListItem>
                    <asp:ListItem Value="5+">5+</asp:ListItem>
                </asp:DropDownList><br>
         </fieldset>
          <fieldset>
                <legend>Monthly Expenses</legend>
                <h2>Please, estimate your monthly expenses</h2>
                
                <label>Food:</label>
                <asp:TextBox runat="server" id="foodCost" placeholder="Groceries/on campus..."></asp:TextBox>
                <asp:RequiredFieldValidator ID="foodCostValidator" runat="server" ControlToValidate="foodCost"   
ErrorMessage="Please enter an estimate" ForeColor="Red"/><br>
                 <br>
                
                <label>Transportation:</label>
                <asp:Textbox runat="server" id="transportationCost" placeholder="i.e. gas, presto pass, uber..."></asp:Textbox>
                <asp:RequiredFieldValidator ID="transportationCostValidator" runat="server" ControlToValidate="transportationCost"   
ErrorMessage="Please enter an estimate" ForeColor="Red"/><br>
                <br>
               
                <label>Entertainment:</label>
                <asp:Textbox runat="server" id="entertainmentCost"></asp:Textbox>
                <asp:RequiredFieldValidator ID="entertainmentCostValidator" runat="server" ControlToValidate="entertainmentCost"   
ErrorMessage="Please enter an estimate" ForeColor="Red"/><br>
                <br>
                
                <label>Boarding:</label>
                <asp:Textbox runat="server" id="boardingCost" placeholder="Including internet/hydro"></asp:Textbox>
                <asp:RequiredFieldValidator ID="boardingCostValidator" runat="server" ControlToValidate="boardingCost"   
ErrorMessage="Please enter an estimate" ForeColor="Red"/><br>
                <br>
                
                <label>Misc:</label>
                <asp:Textbox runat="server" id="miscCost"></asp:Textbox>
                <asp:RequiredFieldValidator ID="miscCostValidator" runat="server" ControlToValidate="miscCost"   
ErrorMessage="Please enter an estimate" ForeColor="Red"/><br>
                
          </fieldset>
              <div id="termsCondition-styling">  
                    <label>I AGREE TO THE TERMS AND CONDITIONS</label>
                    <br>
                    <asp:RadioButtonList runat="server" ID="termsConditions" CssClass="radiobutton">
                        <asp:ListItem Value="Agree" Text="Agree"> I agree</asp:ListItem>
                        <asp:ListItem Value="Disagree" Text="Disagree"> I disagree</asp:ListItem>
                    </asp:RadioButtonList><br>
             <asp:CustomValidator runat="server" ErrorMessage="Sorry you must agree to the terms" ControlToValidate="termsConditions" OnServerValidate="TermsConditions_Validator"></asp:CustomValidator>
              </div>  
            <div id="submitButton">
                    <asp:Button id="buttonRegister" runat="server" Text="Register" OnClick="Registrations" CssClass="button"></asp:Button>
                    
            </div>
                <asp:ValidationSummary id="ValidationSummary" runat="server" ForeColor="Blue"></asp:ValidationSummary>
            <div runat="server" id="StudentSum">
            
            </div>
            <div runat="server" id="Summary">
            
            </div>
            <div runat="server" id="GrantQualify">
                
            </div>
        </form>
</body>
</html>

<!--Regular Expression for Email found at:
https://www.javatpoint.com/asp-net-web-form-regular-expression-validator

Regular Expression for Postal Code found at:
https://stackoverflow.com/questions/1146202/canadian-postal-code-validation 
-->

