﻿using System;
using System.Collections.Generic;
namespace oct
{
    public class Budget
    {
        //info that I want in the budget object
        //food
        //transpo
        //entertainement
        //boarding
        //misc 
        private double foodCost;
        private double transportationCost;
        private double entertainmentCost;
        private double boardingCost;
        private double miscCost;

        public Budget()
        {

        }

        public double FoodCost
        {
            get { return foodCost; }
            set { foodCost = value; }
        }
        public double TransportationCost
        {
            get { return transportationCost; }
            set { transportationCost = value; }
        }
        public double EntertainmentCost
        {
            get { return entertainmentCost; }
            set { entertainmentCost = value; }
        }
        public double BoardingCost
        {
            get { return boardingCost; }
            set { boardingCost = value; }
        }
        public double MiscCost
        {
            get { return miscCost; }
            set { miscCost = value; }
        }
    }
}
