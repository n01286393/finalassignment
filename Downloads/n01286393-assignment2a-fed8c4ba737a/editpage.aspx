﻿<%@ Page Title="" Language="C#" MasterPageFile="~/Site.Master" AutoEventWireup="true" CodeBehind="editpage.aspx.cs" Inherits="assignment2a.editpage" %>
<asp:Content runat="server" ContentPlaceHolderID="mainContent">
<asp:Panel id="pnl_edit_page" runat="server" Visible="true">
<asp:SqlDataSource runat="server" id="page_select"
        ConnectionString="<%$ ConnectionStrings:crud %>">
</asp:SqlDataSource>
<asp:SqlDataSource runat="server" id="edit_page"
        ConnectionString="<%$ ConnectionStrings:crud %>">
</asp:SqlDataSource>

<h3 runat="server" id="page_name">Update </h3>
    <div>
        <label>Title:</label>
        <asp:RequiredFieldValidator 
                id="val_page_title_edit" 
                runat="server" 
                ControlToValidate="page_title"
                Text="*"
                ErrorMessage="Please enter your page title">
            </asp:RequiredFieldValidator>
        <asp:textbox id="page_title" runat="server">
        </asp:textbox>
    </div>
    <div>
        <label>Author:</label>
        <uctrl:AuthorPick id="page_author" runat="server"></uctrl:AuthorPick>      
    </div>
    <div>
        <label>Category:</label>
        <uctrl:CategoryPick id="page_category" runat="server"></uctrl:CategoryPick>
    </div>

    <div>
        <label>Main Content:</label>
        <br>
        <asp:RequiredFieldValidator 
                id="val_page_main_content_edit" 
                runat="server" 
                ControlToValidate="page_main_content"
                Text="*"
                ErrorMessage="Please enter your content">
            </asp:RequiredFieldValidator>
        <asp:TextBox 
                runat="server" 
                id="page_main_content"
                style="resize:none" 
                TextMode="MultiLine"
                Width="500px"
                Rows="10">
            </asp:TextBox>
    </div>
    
     <div>
        <label>Is your post ready to be published?</label>
        <asp:RequiredFieldValidator 
                id="val_page_edit" 
                Display="Dynamic"
                runat="server" 
                ControlToValidate="page_publish"
                Text="*"
                ErrorMessage="Please specify if you would like this post to be published">
            </asp:RequiredFieldValidator>
        <asp:RadioButtonList runat="server" id="page_publish">
                <asp:ListItem Value="N" Text="No, keep private"></asp:ListItem>
                <asp:ListItem Value="Y" Text="Yes, publish"></asp:ListItem>
        </asp:RadioButtonList>
    </div>
     
    <div>
     <asp:Button Text="Edit Page" runat="server" OnClick="Edit_Page"/>
    </div>
</asp:Panel>
<asp:Panel id="pnl_edit_page_successMessage" runat="server" Visible="false">
   <h3>You have just edited your page. Please have a look at more pages <a href="manage.aspx">here</a>.</h3>
</asp:Panel>
</asp:Content>