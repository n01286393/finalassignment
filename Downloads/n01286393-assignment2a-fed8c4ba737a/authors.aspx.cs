﻿using System;
using System.Data;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Configuration;
namespace assignment2a
{
    public partial class authors : System.Web.UI.Page
    {
        private string basequery = "SELECT Authors.Name, Pages.author_id FROM Authors LEFT JOIN Pages ON Authors.author_id = Pages.author_id GROUP BY Authors.Name, Pages.author_id";
    
            protected void Page_Load(object sender, EventArgs e)
            {
                authors_select.SelectCommand = basequery;
                authors_list.DataSource = Authors_Manual_Bind(authors_select);
                authors_list.DataBind();
    
            }
            
            protected DataView Authors_Manual_Bind(SqlDataSource src)
            {
                DataTable mytbl;
                DataView myview;
                mytbl = ((DataView)src.Select(DataSourceSelectArguments.Empty)).Table;
                foreach (DataRow row in mytbl.Rows)
                {

                row["Name"] =
                    "<a href=\"author.aspx?author_id="
                    + row["author_id"]
                    + "\">"
                    + row["Name"]
                    + "</a>";
                }
            mytbl.Columns.Remove("author_id");
            myview = mytbl.DefaultView;

            return myview;
    
            }
    }
}
