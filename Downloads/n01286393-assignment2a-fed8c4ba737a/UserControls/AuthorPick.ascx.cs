﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Data;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;

namespace assignment2a.UserControls
{

    public partial class AuthorPick : System.Web.UI.UserControl
    {
        private string basequery = "SELECT author_id, Name FROM Authors";

        private int selected_id;
        public int _selected_id
        {
            get { return selected_id; }
            set { selected_id = value; }
        }
        protected void Page_Load(object sender, EventArgs e)
        {

            authors_list_pick.SelectCommand = basequery;
            AuthorPick_Manual_Bind(authors_list_pick, "author_pick");
            
        }

        void AuthorPick_Manual_Bind(SqlDataSource src, string ddl_id)
        {


            DropDownList authorlist = (DropDownList)FindControl(ddl_id);
            authorlist.Items.Clear();
            DataView myview = (DataView)src.Select(DataSourceSelectArguments.Empty);

            foreach (DataRowView row in myview)
            {
                ListItem author_item = new ListItem();
                author_item.Text = row["Name"].ToString();
                author_item.Value = row["author_id"].ToString();
                authorlist.Items.Add(author_item);
                authorlist.DataBind();
            }
        }

        protected void Assign_AuthorID(object sender, EventArgs e)
        {
            _selected_id = int.Parse(author_pick.SelectedValue);
            
        }
    }
}
