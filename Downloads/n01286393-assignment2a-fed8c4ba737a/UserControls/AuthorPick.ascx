﻿<%@ Control Language="C#" AutoEventWireup="true" CodeBehind="AuthorPick.ascx.cs" Inherits="assignment2a.UserControls.AuthorPick" %>
<asp:SqlDataSource 
    runat="server" 
    ID="authors_list_pick"
    ConnectionString="<%$ ConnectionStrings:crud %>">
</asp:SqlDataSource>
<asp:DropDownList 
    runat="server" 
    ID="author_pick"
    OnSelectedIndexChanged="Assign_AuthorID">
</asp:DropDownList>
