﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Data;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;

namespace assignment2a.UserControls
{

    public partial class PagesNav : System.Web.UI.UserControl
    {

        private string basequery = "SELECT page_id, title FROM Pages WHERE publish='Y'";
        
        protected void Page_Load(object sender, EventArgs e)
        {
            pages_list.SelectCommand = basequery;
            MakeNavigation(pages_list);
        }
        
        void MakeNavigation(SqlDataSource src)
        {
            string nav = "";
            DataView pagesListView = (DataView)src.Select(DataSourceSelectArguments.Empty);
            
            foreach (DataRowView row in pagesListView)
            {
                nav += "<li><a href=\"/page.aspx?page_id=" + row["page_id"] + "\">" + row["title"] + "</a></li>";
            }

            pageNav.InnerHtml = nav;
        }
    
    }
}
