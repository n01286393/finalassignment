﻿<%@ Control Language="C#" AutoEventWireup="true" CodeBehind="CategoryPick.ascx.cs" Inherits="assignment2a.UserControls.CategoryPick" %>
<asp:SqlDataSource 
    runat="server" 
    ID="categories_list_pick"
    ConnectionString="<%$ ConnectionStrings:crud %>">
</asp:SqlDataSource>
<asp:DropDownList 
    runat="server" 
    ID="category_pick"
    OnSelectedIndexChanged="OnSelectedIndexChanged">
</asp:DropDownList>