﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Data;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;

namespace assignment2a.UserControls
{

    public partial class CategoryPick : System.Web.UI.UserControl
    {
        private string basequery = "SELECT categoryid as id," + "description as category from categories";

        private int selected_id;
        public int _selected_id
        {
            get { return selected_id; }
            set { selected_id = value; }
        }
        protected void Page_Load(object sender, EventArgs e)
        {
            categories_list_pick.SelectCommand = basequery;
            CategoryPick_Manual_Bind(categories_list_pick, "category_pick");
        }

        void CategoryPick_Manual_Bind(SqlDataSource src, string ddl_id)
        {
            DropDownList categorylist = (DropDownList)FindControl(ddl_id);
            categorylist.Items.Clear();
            DataView myview = (DataView)src.Select(DataSourceSelectArguments.Empty);

            //this loop is taken from Christine
            foreach (DataRowView row in myview)
            {
                ListItem category_item = new ListItem
                {
                    Text = row["category"].ToString(),
                    Value = row["id"].ToString()
                };
                categorylist.Items.Add(category_item);
                categorylist.DataBind(); //had to add in the databind myself
            }
        }

        protected void OnSelectedIndexChanged(object sender, EventArgs e)
        {
            _selected_id = int.Parse(category_pick.SelectedItem.Value);
            
        }
    }
}
