﻿<%@ Page Language="C#" Title="author" MasterPageFile="~/Site.master" AutoEventWireup="true" CodeBehind="author.aspx.cs" Inherits="assignment2a.author" %>
<asp:Content runat="server" ContentPlaceHolderID="mainContent">
<h3 id="author_name" runat="server"></h3>
   <asp:SqlDataSource runat="server"
        id="author_select"
        ConnectionString="<%$ ConnectionStrings:crud %>">
    </asp:SqlDataSource>
        <asp:SqlDataSource runat="server"
        id="author_page_select"
        ConnectionString="<%$ ConnectionStrings:crud %>">
    </asp:SqlDataSource>
    <asp:DataGrid ID="author_list" runat="server" Width="250px">
    </asp:DataGrid>
    <h4>Here is a list of the pages this author has worked on.</h4>
    <p>Explore its content by clicking on the title.</p>
    <asp:DataGrid ID="author_page_list" runat="server" Width="500px">
    </asp:DataGrid>
</asp:Content>