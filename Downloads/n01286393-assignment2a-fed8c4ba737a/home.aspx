﻿<%@ Page Language="C#" Title="PagePress Home" MasterPageFile="~/Site.master" AutoEventWireup="true" CodeBehind="home.aspx.cs" Inherits="assignment2a.home" %>
<asp:Content runat="server" ContentPlaceHolderID="mainContent">

    <section id="intro">
        <h1>Let's democratize publishing</h1>
        <h2><img id="logo-main" src="images/web-apps-logo.png" alt="PagePress Logo">Pagepress.com</h2>
        <h3>At PagePress you have the freedom to build, change and manage your content.</h3>
    </section>
    <main>
    </main>
</asp:Content>