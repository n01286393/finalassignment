﻿using System;
using System.Data;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Configuration;

namespace assignment2a
{
    public partial class manage : System.Web.UI.Page
    { 
            //private string basequery = "SELECT page_id, Authors.author_id, title as Title, page_date as Date, Authors.Name as Author FROM PAGES JOIN Authors on Authors.author_id = Pages.author_id";
            private string basequery = "SELECT page_id, Pages.title as Title, Pages.author_id, Authors.Name, categories.description as Category, CONVERT(varchar(10),Pages.page_date,103) as Date FROM Pages JOIN Authors ON Authors.author_id = Pages.author_id JOIN categories ON categories.categoryid = Pages.category_id";
    
            protected void Page_Load(object sender, EventArgs e)
            {
                pages_select.SelectCommand = basequery;
                pages_list.DataSource = Pages_Manual_Bind(pages_select);
                pages_list.DataBind();
    
            }
            
            //Need a link to the authors
            //Need a link to page
            //Need a link to categories 
            protected DataView Pages_Manual_Bind(SqlDataSource src)
            {
                DataTable mytbl;
                DataView myview;
                mytbl = ((DataView)src.Select(DataSourceSelectArguments.Empty)).Table;
                foreach (DataRow row in mytbl.Rows)
                {
                //Intercept a specific column rendering
                //add a link of that column info
                row["Title"] =
                    "<a href=\"page.aspx?page_id="
                    + row["page_id"]
                    + "\">"
                    + row["Title"]
                    + "</a>";

                row["Name"] =
                    "<a href=\"author.aspx?author_id="
                    + row["author_id"]
                    + "\">"
                    + row["Name"]
                    + "</a>";

            }
            mytbl.Columns.Remove("page_id");
            mytbl.Columns.Remove("author_id");
            myview = mytbl.DefaultView;

            return myview;
    
            }
    
    }
}
