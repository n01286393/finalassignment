﻿<%@ Page Language="C#" MasterPageFile="~/Site.master" AutoEventWireup="true" CodeBehind="manage.aspx.cs" Inherits="assignment2a.manage" %>
<asp:Content runat="server" ContentPlaceHolderID="mainContent">
<h1>Manage your blog's pages</h1>
<p>Click <a href="createpage.aspx">here</a> to create some more pages.</p>
<p>Explore an individual page by clicking on its title or explore an author's work by pressing on their name!</p>
<asp:SQLDataSource 
    id="pages_select" 
    runat="server"  
    ConnectionString="<%$ ConnectionStrings:crud %>">
    </asp:SQLDataSource>
    <asp:DataGrid runat="server" id="pages_list"></asp:DataGrid>
</asp:Content>