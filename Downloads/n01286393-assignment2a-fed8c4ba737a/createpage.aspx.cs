﻿using System;
using System.Collections.Generic;
using System.Configuration;
using System.Data;
using System.Data.SqlClient;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Globalization;
namespace assignment2a
{
    public partial class createpage : System.Web.UI.Page
    {
        
        private string addquery = "INSERT INTO Pages (title,author_id,category_id,content,publish,page_date) VALUES ";      

        protected void Page_Load(object sender, EventArgs e)
        {
        
        }

        protected void page_submit_Click(object sender, EventArgs e)
        {
            if (Page.IsValid)
            {
                pnl_create_page.Visible = false;
                pnl_create_page_successMessage.Visible = true;


                string title = page_titlee.Text;
                int authorName = page_author._selected_id;
                string pagedate = DateTime.Now.ToString();
                int category_id = page_category._selected_id;
                string main = page_main_content.Text;
                string publish = page_publish.SelectedValue;

                addquery += "('" + title + "','" + authorName + "','" + category_id + "','" + main + "','" + publish + "','" + pagedate + "')";

                insert_page.InsertCommand = addquery;
                insert_page.Insert();
            }

        }

    }
}
