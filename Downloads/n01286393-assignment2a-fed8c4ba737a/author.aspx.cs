﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Data;
using System.Web.UI;
using System.Web.UI.WebControls;
namespace assignment2a
{
    public partial class author : System.Web.UI.Page
    {
        
        private string author_basequery = "SELECT Name FROM Authors";

        private string author_page_basequery =
            "SELECT Pages.page_id, Pages.title from Authors " +
            " left join Pages on Authors.author_id = Pages.author_id ";

        protected void Page_Load(object sender, EventArgs e)
        {
            string author_id = Request.QueryString["author_id"];
            if (string.IsNullOrEmpty(author_id)) author_name.InnerHtml = "No author found.";
            else
            {
                author_basequery += " WHERE AUTHOR_ID = " + author_id;
                author_select.SelectCommand = author_basequery;

                DataView authorview = (DataView)author_select.Select(DataSourceSelectArguments.Empty);
                DataRowView authorrowview = authorview[0];
                string authorname = authorview[0]["Name"].ToString();
                author_name.InnerHtml = authorname;

                author_list.DataSource = author_select;
                author_list.DataBind();

                author_page_basequery += " WHERE AUTHORS.AUTHOR_ID=" + author_id;
                author_page_select.SelectCommand = author_page_basequery;

                author_page_list.DataSource = Author_Manual_Bind(author_page_select);
                author_page_list.DataBind();

            }
        }

        protected DataView Author_Manual_Bind(SqlDataSource src)
        {
            DataTable mytbl;
            DataView myview;
            mytbl = ((DataView)src.Select(DataSourceSelectArguments.Empty)).Table;
            foreach (DataRow row in mytbl.Rows)
            {
                row["Title"] =
                    "<a href=\"page.aspx?page_id="
                    + row["page_id"]
                    + "\">"
                    + row["Title"]
                    + "</a>";
            }
            mytbl.Columns.Remove("page_id");
            myview = mytbl.DefaultView;

            return myview;

        }
        
    }
}
