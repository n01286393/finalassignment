﻿<%@ Page Language="C#" Title="Authors" MasterPageFile="~/Site.master" AutoEventWireup="true" CodeBehind="authors.aspx.cs" Inherits="assignment2a.authors" %>
<asp:Content runat="server" ContentPlaceHolderID="mainContent">
<h1>Here's a list of your contributing authors</h1>
<p>Select an author's name to see the work that they've published in more detail.</p>
<asp:SQLDataSource 
    id="authors_select" 
    runat="server"  
    ConnectionString="<%$ ConnectionStrings:crud %>">
    </asp:SQLDataSource>
<asp:DataGrid runat="server" id="authors_list"></asp:DataGrid>
</asp:Content>