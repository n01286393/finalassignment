﻿<%@ Page Language="C#" Title="page" MasterPageFile="~/Site.master" AutoEventWireup="true" CodeBehind="page.aspx.cs" Inherits="assignment2a.page" %>
<asp:Content runat="server" ContentPlaceHolderID="mainContent">
<asp:Panel id="pnl_delete_page" runat="server" Visible="true">

<h3 id="page_name" runat="server"></h3>

<p>If you want, you can edit or delete this page.</p>
<asp:Button runat="server" id="del_page_btn"
        OnClick="DelPage"
        OnClientClick="if(!confirm('Are you sure?')) return false;"
        Text="Delete Page" />
<a href="editpage.aspx?page_id=<%Response.Write(this.page_id);%>">Edit Page</a>


<asp:SqlDataSource runat="server"
        id="page_select"
        ConnectionString="<%$ ConnectionStrings:crud %>">
</asp:SqlDataSource>
<asp:DataGrid ID="page_list" runat="server">
</asp:DataGrid>
<asp:SqlDataSource 
        runat="server"
        id="del_page"
        ConnectionString="<%$ ConnectionStrings:crud %>">
</asp:SqlDataSource>
</asp:Panel>
<asp:Panel id="pnl_delete_page_successMessage" runat="server" Visible="false">
   <h3>You just deleted a page. You can create more <a href="createpage.aspx">here.</a></h3>
</asp:Panel>
</asp:Content>