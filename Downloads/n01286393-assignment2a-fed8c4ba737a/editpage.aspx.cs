﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Data;
using System.Web.UI.WebControls;
namespace assignment2a
{
    public partial class editpage : System.Web.UI.Page
    {
    
        public int page_id
        {
            get { return Convert.ToInt32(Request.QueryString["page_id"]); }
        }
        

        protected void Page_Load(object sender, EventArgs e)
        {
            
        }
        protected override void OnPreRender(EventArgs e)
        {
            base.OnPreRender(e);
            DataRowView pagerow = getPageInfo(page_id);
            if (pagerow == null)
            {
                page_name.InnerHtml = "No Page Found.";
                return;
            }
            page_title.Text = pagerow["title"].ToString();
            page_main_content.Text = pagerow["content"].ToString();
            page_publish.SelectedValue = pagerow["publish"].ToString();
            if (!Page.IsPostBack)
            {
                page_name.InnerHtml += page_title.Text;
                
            }

        }

        protected void Edit_Page(object sender, EventArgs e)
        {
            if (Page.IsValid)
            {
                pnl_edit_page.Visible = false;
                pnl_edit_page_successMessage.Visible = true;

                string title = page_title.Text;
                string content = page_main_content.Text;
                int authorName = page_author._selected_id;
                int category_id = page_category._selected_id;
                string publish = page_publish.SelectedValue.ToString();

                string editquery = "Update pages set title='" + title + "'," + "content='" + content + "',author_id='" + authorName + "',category_id='" + category_id + "',publish='" + publish + "' where page_id=" + page_id;
                edit_page.UpdateCommand = editquery;
                edit_page.Update();
            }
            
        }
        protected DataRowView getPageInfo(int id)
        {
            string query = "select * from pages where page_id="+page_id.ToString();
            page_select.SelectCommand = query;
            DataView pageview = (DataView)page_select.Select(DataSourceSelectArguments.Empty);

            if (pageview.ToTable().Rows.Count < 1)
            {
                return null;
            }
            DataRowView pagerowview = pageview[0];
            return pagerowview;

        }   
    
    }
}
