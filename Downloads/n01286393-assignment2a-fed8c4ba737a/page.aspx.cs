﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Data;
using System.Web.UI;
using System.Web.UI.WebControls;
namespace assignment2a
{
    public partial class page : System.Web.UI.Page
    {
    
    //export this to be property of the entire page
        public string page_id {
            get { return Request.QueryString["page_id"]; }
        }
        private string page_basequery = "SELECT page_id, Pages.title as Title, Pages.author_id, Authors.Name, categories.description as Category, Pages.content, Pages.publish, CONVERT(varchar(10),Pages.page_date,103) as Date FROM Pages LEFT JOIN Authors ON Authors.author_id = Pages.author_id LEFT JOIN categories ON categories.categoryid = Pages.category_id";

        protected void Page_Load(object sender, EventArgs e)
        {
            
            if (page_id == "" || page_id == null) page_name.InnerHtml = "No page found.";
            else
            {
                //Need to use custom rendering on this one to get the class link
                page_basequery += " WHERE PAGE_ID = " + page_id;
                //debug.InnerHtml = student_basequery;
                page_select.SelectCommand = page_basequery;

                //Another small manual manipulation to present the students name outside of
                //the datagrid.
                DataView classview = (DataView)page_select.Select(DataSourceSelectArguments.Empty);
                //Since we're operating on a primary key we can guarantee we only get 1 row
                DataRowView studentrowview = classview[0]; //gets the first result which is always the student
                string pagename = classview[0]["Title"].ToString();
                page_name.InnerHtml = pagename;

                page_list.DataSource = Page_Manual_Bind(page_select);
                page_list.DataBind();

            }
        }
        protected DataView Page_Manual_Bind(SqlDataSource src)
        {
            //We should have full control over the ability to pick
            //and choose how we represent our serverside data on the client side HTML

            DataTable mytbl;
            DataView myview;
            mytbl = ((DataView)src.Select(DataSourceSelectArguments.Empty)).Table;
            foreach (DataRow row in mytbl.Rows)
            {
                //Intercept a specific column rendering
                //add a link of that column info
                row["Name"] =
                    "<a href=\"author.aspx?author_id="
                    + row["author_id"]
                    + "\">"
                    + row["Name"]
                    + "</a>";

            }
            mytbl.Columns.Remove("page_id");
            mytbl.Columns.Remove("author_id");
            myview = mytbl.DefaultView;

            return myview;
        }

        protected void DelPage(object sender, EventArgs e)
        {
            pnl_delete_page.Visible = false;
            pnl_delete_page_successMessage.Visible = true;
            
            string delquery = "DELETE FROM PAGES WHERE page_id="+page_id;

            del_page.DeleteCommand = delquery;
            del_page.Delete();

        }
    
    }
}
