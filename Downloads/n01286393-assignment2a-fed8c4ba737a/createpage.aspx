﻿<%@ Page Language="C#" Title="Create Page" Async="true" MasterPageFile="~/Site.master" AutoEventWireup="true" CodeBehind="createpage.aspx.cs" Inherits="assignment2a.createpage" %>
<asp:Content runat="server" ContentPlaceHolderID="mainContent">
<asp:Panel id="pnl_create_page" runat="server" Visible="true">
<h2>Create a New Page</h2>
<div>
            <label>Title:</label>
            <asp:RequiredFieldValidator 
                id="val_page_title" 
                runat="server" 
                ControlToValidate="page_titlee"
                Text="*"
                ErrorMessage="Please enter your page title">
            </asp:RequiredFieldValidator>
            <asp:TextBox 
                runat="server" 
                id="page_titlee"
                MaxLength="200"
                Width="400">
            </asp:TextBox>
        </div>
        
<div>
        <label>Author:</label>
                <uctrl:AuthorPick id="page_author" runat="server"></uctrl:AuthorPick>      
        </div>
        <div>
        <label>Category:</label>
                <uctrl:CategoryPick id="page_category" runat="server"></uctrl:CategoryPick>
        </div>
        
        <div>
            <label>Main Content:</label>
            <asp:RequiredFieldValidator 
                id="val_page_main_content" 
                runat="server" 
                ControlToValidate="page_main_content"
                Text="*"
                ErrorMessage="Please enter your content">
            </asp:RequiredFieldValidator>
            <br>
            <asp:TextBox 
                runat="server" 
                id="page_main_content"
                style="resize:none" 
                TextMode="MultiLine"
                Width="500px"
                Rows="10">
            </asp:TextBox>
        </div>
        <div>
            <label>Is your post ready to be published?</label>
            <asp:RequiredFieldValidator 
                id="val_page_publish" 
                Display="Dynamic"
                runat="server" 
                ControlToValidate="page_publish"
                Text="*"
                ErrorMessage="Please specify if you would like this post to be published">
            </asp:RequiredFieldValidator>
            <asp:RadioButtonList runat="server" id="page_publish">
                <asp:ListItem Value="N" Text="No, keep private"></asp:ListItem>
                <asp:ListItem Value="Y" Text="Yes, publish"></asp:ListItem>
            </asp:RadioButtonList>
        </div>
        
        <div>
        <asp:Button id="page_submit" runat="server" Text="Submit Changes" OnClick="page_submit_Click"></asp:Button>
        </div>
        <div>
            <asp:ValidationSummary id="CreateValidationSummary" runat="server" />
        </div>
</asp:Panel>
<asp:Panel id="pnl_create_page_successMessage" runat="server" Visible="false">
   <h3>Congragulations, you just made a submission. You can create another page <a href="createpage.aspx">here.</a></h3>
<asp:SqlDataSource runat="server" id="insert_page"
        ConnectionString="<%$ ConnectionStrings:crud %>">
</asp:SqlDataSource>
</asp:Panel>
</asp:Content>